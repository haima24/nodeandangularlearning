'use strict';
const db=require('../models/index');

/**
 * Add a product
 * 
 *
 * body Product Product need to be add
 * returns List
 **/
exports.addProduct = async(product)=>{
  // get transaction
  let transaction = await db.sequelize.transaction();
  
  try {
      // step 2
      await db.Product.create(product, {transaction});
      // commit
      await transaction.commit();

      const o=await db.Product.findAll({}, {transaction});
      return o;
    } catch (err) {
      console.log(err);
      // Rollback transaction if any errors were encountered
      await transaction.rollback();
    }
}


/**
 * Deletes a Product
 * 
 *
 * productId Integer Product id to delete
 * returns List
 **/
exports.deleteProduct = async(id)=>{
  let transaction = await db.sequelize.transaction();
  try {
      // step 2
      await db.Product.destroy(
        {where:{id:id} },
         {transaction});
      const o=await db.Product.findAll({}, {transaction});
      
      // commit
      await transaction.commit();

      return o;
    } catch (err) {
      console.log(err);
      // Rollback transaction if any errors were encountered
      await transaction.rollback();
    }
}


/**
 * Get all product
 * 
 *
 * returns List
 **/
exports.getAllProduct = async()=>{
  // get transaction
let transaction = await db.sequelize.transaction();
try {
  // step 2
  const o=await db.Product.findAll({}, {transaction});
  // commit
  await transaction.commit();
  return o;
} catch (err) {
  console.log(err);
  // Rollback transaction if any errors were encountered
  await transaction.rollback();
}
}


/**
 * Update an existing product
 * 
 *
 * body Product Product need to be update
 * returns List
 **/
exports.updateProduct = async(product)=>{
  let transaction = await db.sequelize.transaction();
  
  try {
      // step 2
      await db.Product.update(product,
        {where:{id:product.id} },
         {transaction});
      const o=await db.Product.findAll({}, {transaction});
      
      // commit
      await transaction.commit();

      return o;
    } catch (err) {
      console.log(err);
      // Rollback transaction if any errors were encountered
      await transaction.rollback();
    }
}

