var express = require('express');
var router = express.Router();
module.exports = (keycloak)=>{
  router.post('/',keycloak.protect(),(req,res,next)=>{
    next();
  });
  router.put('/',keycloak.protect(),(req,res,next)=>{
    next();
  });
  router.delete('/',keycloak.protect(),(req,res,next)=>{
    next();
  });
  return router;
};
