import { BrowserModule } from '@angular/platform-browser';
import { NgModule,DoBootstrap, ApplicationRef  } from '@angular/core';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgProductComponent } from '../screens/ng-product/ng-product.component';
import {HttpClientModule} from '@angular/common/http';
import { ProductService } from '../services/product';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { environment } from '../environments/environment';

const keycloakService = new KeycloakService();

@NgModule({
  declarations: [
    AppComponent,
    NgProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    KeycloakAngularModule
  ],
  providers: [ProductService,
    {
      provide: KeycloakService,
      useValue: keycloakService,
    }],
    entryComponents: [AppComponent],
})
export class AppModule implements DoBootstrap {
  async ngDoBootstrap(app) {
    // const { keycloakConfig } = environment;

    try {
      keycloakService.init({
        config: {
          url: environment.authUrl,
          realm: 'nodejs-example',
          clientId: 'nodejs-connect',
        },
        initOptions: {
          onLoad: 'login-required',
          checkLoginIframe: false
        },
        enableBearerInterceptor: true
        // ,
        // bearerExcludedUrls: ['.*']
      }).then(() => {
        app.bootstrap(AppComponent);
      });
    } catch (error) {
      console.error('Keycloak init failed', error);
    }
  }
}