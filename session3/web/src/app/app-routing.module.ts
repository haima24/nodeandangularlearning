import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgProductComponent } from '../screens/ng-product/ng-product.component';
import { CanAuthenticationGuard } from '../../guard/CanAuthenticationGuard'; 

const routes: Routes = [
  { path: '', redirectTo: '/product', pathMatch: 'full'},
  { path: 'product', component: NgProductComponent,canActivate:[CanAuthenticationGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
