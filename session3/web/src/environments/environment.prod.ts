export const environment = {
  production: true,
  baseAPIUrl: 'http://host.docker.internal:3001/api/',
  authUrl:'http://localhost:8081/auth'
};
