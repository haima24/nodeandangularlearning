export const environment = {
  production: false,
  baseAPIUrl: 'http://localhost:3000/api/',
  authUrl:'http://localhost:8080/auth'
};