import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ng-product',
  templateUrl: './ng-product.component.html',
  styleUrls: ['./ng-product.component.css']
})
export class NgProductComponent implements OnInit {

  dt:any=[];
  closeResult = '';
  formClass='needs-validation';
  modalTitle="Add new product";
  modalType="create";
  product={name:'',price:0}
  constructor(private _p:ProductService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    this._p.getAll().subscribe(
      (data)=>{
      this.dt=data;
    },(err)=>{
      console.log("something went wrong!"+err.message);
    });
  }
  delete(id){
    this._p.deleteProduct(id).subscribe(
      (data)=>{
      this.dt=data;
    },(err)=>{
      console.log("something went wrong!"+err.message);
    });
  }
  open(content,type,p) {
    if(type=='create'){
      this.modalTitle="Add new product";
    }else{
      this.modalTitle="Update Product";
    }
    this.modalType=type;
    this.product=Object.assign({}, p);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.formClass='needs-validation';
      this.product={name:'',price:0}
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.formClass='needs-validation';
      this.product={name:'',price:0}
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  save(f,m){
    this.formClass='was-validated';
    if(f.form.valid){
      if(this.modalType=='create'){
        this._p.createProduct(this.product).subscribe(
          (data)=>{
            debugger;
          this.dt=data;
        },(err)=>{
          console.log("something went wrong!"+err.message);
        });
      }else{
        this._p.updateProduct(this.product).subscribe(
          (data)=>{
          this.dt=data;
        },(err)=>{
          console.log("something went wrong!"+err.message);
        });
      }
      
      m.close('save click');
    }
  }
  
}
