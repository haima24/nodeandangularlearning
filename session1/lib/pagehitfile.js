const httpReferrer = require('./httpreferrer'),
fileAccess=new (require('./fileaccess'))('testfile.txt');
module.exports = class {

    // // initialize
    // constructor() {
    //     // counter storage
    //     this.counter = {};
    // }

    // increase URL counter
    count(req,cb) {

        let hash = httpReferrer(req);
        
        if (!hash) return null;

        fileAccess.read((dt=>{
            let counter={};

            if(dt){
                counter=JSON.parse(dt);
            }
            // define count default
            counter[ hash ] = counter[ hash ] || 0;
            
            ++counter[hash];
            cb(counter[hash]);
            let counterToFile=JSON.stringify(counter);
            fileAccess.write(counterToFile);
            
        }));

        
    }

};