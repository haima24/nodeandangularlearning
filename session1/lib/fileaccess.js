'use strict';

const
    fs = require('fs'),
    fsPromise = fs.promises;
module.exports = class {
    constructor(fileName) {
        // counter storage
        this.file = fileName;
    }
    read=(onRead)=>{
        fsPromise.readFile(this.file,{encoding:'utf-8',flag:'a+'})
        .then((dt) => onRead(dt))
        .catch(err => console.log('file read failed:', err))
    }
    write=(value)=>{
        fsPromise.writeFile(this.file,value)
        .then(() => console.log('done'))
        .catch(err => console.log('file read failed:', err))
    }
}



