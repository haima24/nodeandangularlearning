const db=require('../models/index');
module.exports=class {

    getAll=async()=>{
            // get transaction
        let transaction = await db.sequelize.transaction();
        try {
            // step 2
            const o=await db.Product.findAll({}, {transaction});
            // commit
            await transaction.commit();
            return o;
          } catch (err) {
            console.log(err);
            // Rollback transaction if any errors were encountered
            await transaction.rollback();
          }
    }
    create=async(product)=>{
      // get transaction
      let transaction = await db.sequelize.transaction();
      
      try {
          // step 2
          await db.Product.create(product, {transaction});
          // commit
          await transaction.commit();

          const o=await db.Product.findAll({}, {transaction});
          return o;
        } catch (err) {
          console.log(err);
          // Rollback transaction if any errors were encountered
          await transaction.rollback();
        }
    }
    update=async(id,product)=>{
      let transaction = await db.sequelize.transaction();
      
      try {
          // step 2
          await db.Product.update(product,
            {where:{id:id} },
             {transaction});
          const o=await db.Product.findAll({}, {transaction});
          
          // commit
          await transaction.commit();

          return o;
        } catch (err) {
          console.log(err);
          // Rollback transaction if any errors were encountered
          await transaction.rollback();
        }
    }

    delete=async(id)=>{
      let transaction = await db.sequelize.transaction();
      try {
          // step 2
          await db.Product.destroy(
            {where:{id:id} },
             {transaction});
          const o=await db.Product.findAll({}, {transaction});
          
          // commit
          await transaction.commit();

          return o;
        } catch (err) {
          console.log(err);
          // Rollback transaction if any errors were encountered
          await transaction.rollback();
        }
    }
}