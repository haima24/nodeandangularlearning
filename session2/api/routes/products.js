var express = require('express');
var router = express.Router();
var productService=new (require('../services/productService'))();
/* GET users listing. */
router.get('/', function(req, res, next) {
  (async()=>{
    const dt=await productService.getAll();
    res.json(dt);
  })()
});
router.post('/',(req,res,next)=>{
  (async()=>{
    const dt=await productService.create(req.body);
    res.json(dt);
  })()
});
router.put('/:id',(req,res,next)=>{
  (async()=>{
    const dt=await productService.update(req.params.id,req.body);
    res.json(dt);
  })()
});

router.delete('/:id',(req,res,next)=>{
  (async()=>{
    const dt=await productService.delete(req.params.id);
    res.json(dt);
  })()
});


module.exports = router;
