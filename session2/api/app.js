// var createError = require('http-errors');
var express = require('express');
// var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var allRoutes = require('./routes/all-routes');

var Keycloak = require('keycloak-connect');
var session = require('express-session');

var memoryStore = new session.MemoryStore();

var app = express();



app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({
  secret: 'myshop',
  resave: false,
  saveUninitialized: true,
  store: memoryStore
}));
var keycloak = new Keycloak({
  store: memoryStore
});

// app.use(express.static(path.join(__dirname, 'public')));
// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.use(keycloak.middleware({
  logout: '/logout',
  admin: '/',
  protected: '/protected/resource'
}));

// app.get('/login', keycloak.protect(), function (req, res) {
//   res.render('index', {
//     result: JSON.stringify(JSON.parse(req.session['keycloak-token']), null, 4),
//     event: '1. Authentication\n2. Login'
//   });
// });
// app.get('/protected/resource', keycloak.enforcer(['resource:view', 'resource:write'], {
//   resource_server_id: 'nodejs-apiserver'
// }), function (req, res) {
//   res.render('index', {
//     result: JSON.stringify(JSON.parse(req.session['keycloak-token']), null, 4),
//     event: '1. Access granted to Default Resource\n'
//   });
// });

allRoutes.forEach(o=>{
  app.use(o.route,keycloak.protect(), require(o.path));
});


// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   // next(createError(404));
//   console.log()
//   next();
// });

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  console.log(err.message);
  
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
