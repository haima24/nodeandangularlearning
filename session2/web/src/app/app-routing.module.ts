import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgProductComponent } from '../screens/ng-product/ng-product.component';

const routes: Routes = [
  { path: '', redirectTo: '/product', pathMatch: 'full'},
  { path: 'product', component: NgProductComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
