import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { environment } from '../environments/environment';


@Injectable()
export class ProductService {

  constructor(private http: HttpClient) { }

  getAll(){
    let uri = environment.baseAPIUrl+'Product';
    return this.http.get(uri);
  }
  createProduct(product:any){
    let uri = environment.baseAPIUrl+'Product';
    let headers= new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(uri,JSON.stringify(product) ,{headers});
  }
  updateProduct(product:any){
    let uri = environment.baseAPIUrl+'Product';
    let headers= new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.put(uri+"/"+product.id,product);
  }
  deleteProduct(id:any){
    let uri = environment.baseAPIUrl+'Product';
    return this.http.delete(uri+"/"+id);
  }
}
